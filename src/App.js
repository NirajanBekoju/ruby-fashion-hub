import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Home from './Containers/Home';
import About from './Containers/About';
import Contact from './Containers/Contact';
import Product from './Containers/Product';
import ProductDetail from './Containers/ProductDetail';
import Search from './Containers/Search';

import NotFound from './Components/NotFound';

import Layout from './HOC/Layout';

import { Provider } from "react-redux";
import store from "./Redux/Store";

const App = () => (
  <Provider store={store}>
    <Router>
      <Layout>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/about" component={About} />
          <Route exact path="/product" component={Product} />
          <Route exact path="/product/:id" component={ProductDetail} />
          <Route exact path="/contact" component={Contact} />
          <Route exact path="/search" component={Search} />
          <Route component={NotFound} />
        </Switch>
      </Layout>
    </Router>
  </Provider>
);


export default App;
