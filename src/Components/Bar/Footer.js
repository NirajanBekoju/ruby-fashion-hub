import React from "react";

function Footer() {
  return (
    <footer className="mt-0">
      <div className="container">
        <div className="row">
          <div className="col-md-4">
            <h4>About Ruby Online Hub</h4>
            <p>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book. It has
              survived not only five centuries, but also the leap into
              electronic typesetting, remaining essentially unchanged.{" "}
              <a href="#">Read More...</a>
            </p>
          </div>
          <div className="col-md-4">
            <h4> Contact Us</h4>
            <p>
              <i class="fa fa-building pr-2" aria-hidden="true"></i>
              Ruby Fashion Hub
              <br />
              <i class="fa fa-user pr-2" aria-hidden="true"></i>
              Ramesh Sapkota <br />
              <i class="fa fa-map-marker pr-2" aria-hidden="true"></i>
              Baneshwor, Kathmandu <br />
              <i className="fa fa-phone pr-2" aria-hidden="true"></i>
              9851186253
              <br />
              <i className="fa fa-envelope pr-2" aria-hidden="true"></i>
              rameshsapkota5253@gmail.com
            </p>
          </div>

          <div className="col-md-4 media-footer">
            <h4>Social Media</h4>
            <a href="https://www.facebook.com/rubyonlinehub/" target="_blank" className="pr-2">
              <i className="fa fa-facebook-official" aria-hidden="true"></i>
            </a>
            <a href="#" className="pr-2">
              <i className="fa fa-instagram" aria-hidden="true"></i>
            </a>
            <a href="#" className="pr-2">
              <i className="fa fa-twitter" aria-hidden="true"></i>
            </a>
            <a href="#" className="pr-2">
              <i className="fa fa-linkedin" aria-hidden="true"></i>
            </a>
          </div>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
