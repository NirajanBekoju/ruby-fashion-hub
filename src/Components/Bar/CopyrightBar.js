import React from "react";

function CopyrightBar() {
  return (
    <section id="copyright-bar">
      <div className="container-fluid">
        <div className="row py-1">
          <div className="col-md-12 text-white text-center">
            &copy; Copyright. All Rights Reserved.
          </div>
        </div>
      </div>
    </section>
  );
}

export default CopyrightBar;
