import React from "react";

function TopBar() {
  return (
    <section id="topBar">
      <div className="container">
        <div className="row py-1">
          <div className="col-md-6 text-white">
            <span>
              <i className="fa fa-phone pr-2" aria-hidden="true"></i>
              <span>9851-186-253</span>
            </span>
            <span>
              <i className="fa fa-envelope pl-2 pr-2" aria-hidden="true"></i>
              <span className="email">rameshsapkota5253@gmail.com</span>
            </span>
          </div>
          <div className="col-md-6 d-flex justify-content-end text-white text-end">
            <div className="pr-2">
              <a href="https://www.facebook.com/rubyonlinehub/" target="_blank">
                <i className="fa fa-facebook-official" aria-hidden="true"></i>
              </a>
            </div>
            <div className="pr-2">
              <i className="fa fa-instagram" aria-hidden="true"></i>
            </div>
            <div className="pr-2">
              <i className="fa fa-twitter" aria-hidden="true"></i>
            </div>
            <div>
              <i className="fa fa-linkedin" aria-hidden="true"></i>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default TopBar;
