import React, {useState} from "react";
import { NavLink, Link } from 'react-router-dom';
import Alert from "../Alert";

import logo from '../../assets/images/logo.png';

function NavBar() {
  const [formData, setFormData] = useState({
    q: ""
  })

  const {search} = formData

  const handleChange = (e) => {
    setFormData((prevFormData) => {
      return {...prevFormData, [e.target.name] : e.target.value}
    })
  }

  return (
    <React.Fragment>
      <section>
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
          <div className="container">
            <img src={logo} alt="Logo" height="50" width="50"></img>
            <Link className="navbar-brand text-white pl-3" to="/">
              Ruby Fashion Hub
            </Link>
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div
              className="collapse navbar-collapse"
              id="navbarSupportedContent"
            >
              <div>
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                  <li className="nav-item">
                    <NavLink
                      activeclassname="activeReddish"
                      className="nav-link"
                      aria-current="page"
                      to="/"
                    >
                      Home
                    </NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink
                      activeclassname="activeReddish"
                      className="nav-link"
                      to="/product"
                    >
                      Products
                    </NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink
                      activeclassname="activeReddish"
                      className="nav-link"
                      to="/about"
                    >
                      About Us
                    </NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink
                      activeclassname="activeReddish"
                      className="nav-link"
                      to="/contact"
                    >
                      Contact Us
                    </NavLink>
                  </li>
                </ul>
              </div>

              <div className="ml-auto">
                <form action="http://127.0.0.1:8000/search/" className="d-flex">
                  <input
                    className="form-control me-2 mr-2"
                    type="search"
                    placeholder="Search any keywords..."
                    aria-label="Search"
                    name="q"
                    value={search}
                    onChange={(e) => handleChange(e)}
                  />
                  <button className="btn btn-outline-success" type="submit">
                    Search
                  </button>
                </form>
              </div>
            </div>
          </div>
        </nav>
      </section>
      <Alert />
    </React.Fragment>
  );
}

export default NavBar;
