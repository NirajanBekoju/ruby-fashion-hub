import React, { useState } from "react";
import axios from "axios";

import { setAlert } from '../../Redux/Alert/AlertAction'
import { connect } from 'react-redux';

function Inquiry_Modal({ product, setAlert }) {
  const product_id = product.id;

  const [formData, setFormData] = useState({
    name: "",
    email: "",
    phone: "",
    address: "",
  });

  const { name, email, phone, address } = formData;

  const handleChange = (e) => {
    setFormData((prevFormData) => {
      return { ...prevFormData, [e.target.name]: e.target.value };
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    axios.defaults.headers = {
      "Content-Type": "application/json",
    };

    axios
      .post("http://127.0.0.1:8000/api/inquiry/", {
        name,
        email,
        product_id,
        phone,
        address,
      })
      .then((res) => {
        document.getElementById("close").click();
        window.scrollTo(0, 0);
        if(res.data["success"]){
          setAlert(`"${product.title}" Inquiry has been Made.`, "success");
        }
        else{
          setAlert("Please fill the form correctly as in given examples.", "danger");  
        }
      })
      .catch((err) => {
        document.getElementById("close").click();
        window.scrollTo(0, 0);
        setAlert("Message Failed To Sent.", "danger");
      });
  };

  return (
    <div
      className="modal fade"
      id="sendMessageModal"
      tabIndex="-1"
      role="dialog"
      aria-labelledby="sendMessageModal"
      aria-hidden="true"
    >
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title">Product Inquiry - {product.title}</h5>
            <button
              type="button"
              className="close"
              id="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <form onSubmit={(e) => handleSubmit(e)}>
              <div className="form-group">
                <label htmlFor="name">Name</label>
                <input
                  type="text"
                  className="form-control"
                  id="name"
                  aria-describedby="emailHelp"
                  placeholder="Enter your name"
                  name="name"
                  value={name}
                  onChange={(e) => handleChange(e)}
                />
              </div>
              <div className="form-group">
                <label htmlFor="email">Email</label>
                <input
                  type="email"
                  name="email"
                  className="form-control"
                  id="email"
                  placeholder="Email"
                  value={email}
                  onChange={(e) => handleChange(e)}
                />
              </div>

              <div className="form-group">
                <label htmlFor="phone">Phone</label>
                <input
                  type="tel"
                  name="phone"
                  className="form-control"
                  id="phone"
                  placeholder="Eg: 98*-***-****"
                  value={phone}
                  onChange={(e) => handleChange(e)}
                />
              </div>

              <div className="form-group">
                <label htmlFor="address">Address</label>
                <input
                  type="text"
                  name="address"
                  className="form-control"
                  id="address"
                  placeholder="Enter your Address"
                  value={address}
                  onChange={(e) => handleChange(e)}
                />
              </div>
              <hr />
              <button type="submit" className="btn btn-success">
                Submit
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    setAlert: (msg, alertType) => dispatch(setAlert(msg, alertType)),
  };
};

export default connect(null, mapDispatchToProps)(Inquiry_Modal);
