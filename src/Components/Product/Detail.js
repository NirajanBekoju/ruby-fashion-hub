import React from 'react'

function Detail({product}) {
    return (
      <section className="bg-light pt-4 pb-4">
        <div className="container">
          <div className="row">
            <div className="offset-md-2 col-md-4">
              <img
                src={product.image}
                className="detail-image img-fluid"
              />
            </div>
            <div className="col-md-6">
              <h4>{product.title}</h4>
              <hr />
              Price :
              <strike style={{color: "black"}}>
                <span className="text-bold cut_price pl-1 pr-1">
                  Rs. {product.cut_price}
                </span>
              </strike>
              <span className="text-bold discount_price">
                Rs. {product.price}
              </span>
              <hr />
              <p>{product.description}</p>
              <button
                type="button"
                className="btn btn-primary"
                data-toggle="modal"
                data-target="#sendMessageModal"
              >
                Send Message
              </button>
            </div>
          </div>
        </div>
      </section>
    );
}

export default Detail
