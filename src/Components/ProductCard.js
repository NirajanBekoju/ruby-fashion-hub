import React from 'react'
import {Link} from 'react-router-dom';

function ProductCard({ product }) {
    return (
      <div className="card" style={{ width: "100%" , maxHeight: "500px" }}>
        <img src={product.image} className="card-img-top" alt="Product Image" />
        <div className="card-body">
          <p className="card-text">{product.title}</p>
          <strike>
            <span className="cut_price">Rs. {product.cut_price}</span>
          </strike>
          <br />
          <span className="discount_price">Rs. {product.price}</span><br />
          <Link to={`/product/${product.id}`} className="btn btn-success">View Details</Link>
        </div>
      </div>
    );
}

export default ProductCard
