import React, {useState, useEffect} from "react";
import ProductCard from './ProductCard';

import axios from 'axios';

function FeaturedProduct() {
    const [offeredProducts, setOfferedProducts] = useState([])
    const [count, setCount] = useState(0)

    useEffect(() => {
        window.scrollTo(0,0);
        const fetchData = async () => {
            try{
                const res = await axios.get(
                  "http://127.0.0.1:8000/api/products/offered/"
                );
                setOfferedProducts(res.data.results)
                setCount(res.data.count);
            }
            catch(err){
            }
        }

        fetchData();
    }, [])

    const displayProducts = () => {
        let display = [];
        let result = [];

        offeredProducts.map(product => {
            return display.push(
                <ProductCard 
                    product = {product} key={product.id}
                />
            )
        })

        result.push(
          <div className="carousel-item active" key="0">
            <div className="d-flex justify-content-between">
              {display[0]}
              {display[1]}
              {display[2]}
            </div>
          </div>
        );

        for (let i = 3; i < count; i += 3){
            result.push(
              <div className="carousel-item" key={i}>
                <div className="d-flex justify-content-between">
                  {display[i]}
                  {display[i + 1] ? display[i + 1] : null}
                  {display[i + 2] ? display[i + 2] : null}
                </div>
              </div>
            );
        }
        return result;
    }

  return (
    <section>
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h2>Special Offer</h2>
          </div>
          <div className="col-md-12">
            <div
              id="clothing-carousel-0"
              className="carousel slide"
              data-ride="carousel"
            >
              <div className="carousel-inner">
                  {displayProducts()}
              </div>
              <a
                className="carousel-control-prev"
                href="#clothing-carousel-0"
                role="button"
                data-slide="prev"
              >
                <span
                  className="carousel-control-prev-icon"
                  aria-hidden="true"
                ></span>
                <span className="sr-only">Previous</span>
              </a>
              <a
                className="carousel-control-next"
                href="#clothing-carousel-0"
                role="button"
                data-slide="next"
              >
                <span
                  className="carousel-control-next-icon"
                  aria-hidden="true"
                ></span>
                <span className="sr-only">Next</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default FeaturedProduct;
