import React, {useState} from 'react'
import axios from 'axios';

import { setAlert } from '../../Redux/Alert/AlertAction'
import { connect } from 'react-redux';

function ContactForm({ setAlert }) {
    const [formData, setFormData] = useState({
        name: "",
        email: "",
        phone: "",
        address: "",
        message: ""
    });

    const {name, email, phone, address, message} = formData

    const handleChange = (e) => {
        setFormData((prevFormData) => {
            return {...prevFormData, [e.target.name]: e.target.value}
        })
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        try{
            axios.defaults.headers = {
                "Content-Type": "application/json",
            };

            axios.post("http://127.0.0.1:8000/api/contact/", {
              name,
              email,
              phone,
              address,
              message,
            })
            .then((res) => {
                window.scrollTo(0,0);
                if(res.data["success"]){
                    setAlert("Message has been sent", "success");
                }
                else{
                    setAlert("Please fill up the Form as shown in Example below", "danger");
                }
            })
        }
        catch(err){
            window.scrollTo(0,0);
        }
    }

    return (
      <section className="bg-mainColor text-white">
        <div className="container">
          <div className="row pt-5 pb-3">
            <div className="col-md-8 contact-form-container">
              <h1 className="title-1-big">
                Contact Us<span> Now</span>
              </h1>
              <p className="help-info">
                How can we help you? Do you have any questions or requests? Let
                us know!
              </p>
              <form onSubmit={(e) => handleSubmit(e)}>
                <label htmlFor="name">Name*</label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Enter your name"
                  name="name"
                  value={name}
                  onChange={(e) => handleChange(e)}
                  required
                />
                <label htmlFor="email" className="pt-3">
                  Email*
                </label>
                <input
                  type="email"
                  className="form-control"
                  placeholder="something@gmail.com"
                  name="email"
                  value={email}
                  onChange={(e) => handleChange(e)}
                  required
                />
                <label htmlFor="phone" className="pt-3">
                  Phone Number*
                </label>
                <input
                  type="tel"
                  className="form-control"
                  placeholder="Example: 98x-333-4444"
                  name="phone"
                  value={phone}
                  onChange={(e) => handleChange(e)}
                  required
                />
                <label htmlFor="address" className="pt-3">
                  Address*
                </label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Enter your Address"
                  name="address"
                  value={address}
                  onChange={(e) => handleChange(e)}
                  required
                />
                <label htmlFor="message" className="pt-3">
                  Enter Your Message*
                </label>
                <textarea
                  name="message"
                  id="message"
                  className="form-control mb-3"
                  cols="30"
                  rows="10"
                  name="message"
                  value={message}
                  onChange={(e) => handleChange(e)}
                  required
                ></textarea>

                <button type="submit" className="form-control btn btn-success">
                  Send
                </button>
              </form>
            </div>
            <div className="col-md-4 detail-contact-container">
              <h3 className="title-yellowish">Our Details</h3>
              <p className="detail-sub-info">
                Use the contact form or get in touch with us directly via phone
                or e-mail.
              </p>
              <div className="detail-info">
                <i className="fa fa-phone" aria-hidden="true"></i>
                <span className="title-info-contact pl-2">
                  24 Hours Hotline
                </span>
                <br />
                <span className="info-contact pl-4">01-5122035</span>
              </div>
              <div className="detail-info pt-3">
                <i class="fa fa-mobile" aria-hidden="true"></i>
                <span className="title-info-contact pl-2">Mobile</span>
                <br />
                <span className="info-contact pl-4">+977 9851186253</span>
              </div>

              <div className="detail-info pt-3">
                <i className="fa fa-envelope" aria-hidden="true"></i>
                <span className="title-info-contact pl-2">Mail Us</span>
                <br />
                <span className="info-contact pl-4">
                  rameshsapkota5253@gmail.com
                </span>
              </div>
              <div className="detail-info pt-3">
                <i className="fa fa-address-card" aria-hidden="true"></i>
                <span className="title-info-contact pl-2">Ruby Online Hub</span>
                <br />
                <span className="info-contact pl-4">
                  {" "}
                  Company Reg. No. 123-345-123
                </span>
              </div>
              <div className="detail-info pt-3">
                <i className="fa fa-map-marker" aria-hidden="true"></i>
                <span className="title-info-contact pl-2">Address</span>
                <br />
                <span className="info-contact pl-4">Baneshwor, Kathmandu</span>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
}

const mapDispatchToProps = (dispatch) => {
  return {
    setAlert: (msg, alertType) => dispatch(setAlert(msg, alertType)),
  };
};

export default connect(null, mapDispatchToProps)(ContactForm);
