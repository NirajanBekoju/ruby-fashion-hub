import React from "react";
import banner1 from "../assets/images/banner1.png";

function Banner() {
  return (
    <section>
      <div className="container-fluid p-0">
        <img src={banner1} width="100%" />
      </div>
    </section>
  );
}

export default Banner;
