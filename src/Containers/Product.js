import React, { useState, useEffect } from "react";
import axios from "axios";
import { Helmet } from "react-helmet";

import ProductCard from "../Components/ProductCard";
import Pagination from "../Components/Pagination";

function Product() {
  // Here listings contains only 9 products since the products per page in the backend is 9.
  const [listings, setListings] = useState([]);
  const [count, setCount] = useState(0);
  const [previous, setPrevious] = useState("");
  const [next, setNext] = useState("");
  const [active, setActive] = useState(1);
  const itemsPerPage = 8;

  useEffect(() => {
    window.scrollTo(0, 0);
    const fetchData = async () => {
      try {
        const res = await axios.get(
          "http://127.0.0.1:8000/api/products/?page=1"
        );
        setListings(res.data.results);
        setCount(res.data.count);
        setPrevious(res.data.previous);
        setNext(res.data.next);
      } catch (err) {
      }
    };

    fetchData();
  }, []);

  const displayListings = () => {
    let display = [];
    let result = [];

    listings.map((listing) => {
      return display.push(<ProductCard product={listing} />);
    });

    for (let i = 0; i < listings.length; i += 4) {
      result.push(
        <div className="row mb-4" key={i}>
          <div className="col-md-3">{display[i]}</div>
          <div className="col-md-3">
            {display[i + 1] ? display[i + 1] : null}
          </div>
          <div className="col-md-3">
            {display[i + 2] ? display[i + 2] : null}
          </div>
          <div className="col-md-3">
            {display[i + 3] ? display[i + 3] : null}
          </div>
        </div>
      );
    }

    return result;
  };

  const visitPage = (page) => {
    axios
      .get(`http://127.0.0.1:8000/api/products/?page=${page}`)
      .then((res) => {
        setListings(res.data.results);
        setPrevious(res.data.previous);
        setNext(res.data.next);
        setActive(page);
      })
      .catch((err) => {});
  };

  const previous_number = () => {
    if (previous !== null) {
      axios
        .get(previous)
        .then((res) => {
          setListings(res.data.results);
          setPrevious(res.data.previous);
          setNext(res.data.next);
          if (previous) {
            setActive(active - 1);
          }
        })
        .catch((err) => {});
    }
  };

  const next_number = () => {
    if (next !== null) {
      axios
        .get(next)
        .then((res) => {
          setListings(res.data.results);
          setPrevious(res.data.previous);
          setNext(res.data.next);
          if (next) {
            setActive(active + 1);
          }
        })
        .catch((err) => {});
    }
  };

  return (
    <section className="bg-light pt-4">
      <Helmet>
        <title>Products - Ruby Online Hub</title>
        <meta
          name="description"
          content="Ruby Online Hub is a convenient forum for online distribution that promises secure and effective support for its customers. It is a verified online delivery platform for our customers to purchase fashion products conveniently."
        ></meta>
      </Helmet>
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h2>Products</h2>
            <Pagination
              itemsPerPage={itemsPerPage}
              count={count}
              visitPage={visitPage}
              previous={previous_number}
              next={next_number}
              active={active}
              setActive={setActive}
            />
          </div>
        </div>
        {displayListings()}
      </div>
    </section>
  );
}

export default Product;
