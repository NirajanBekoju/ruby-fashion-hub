import axios from "axios";
import React, { useState, useEffect } from "react";
import { Helmet } from "react-helmet";

import Detail from "../Components/Product/Detail";
import Inquiry_Modal from "../Components/Product/Inquiry_Modal";

function ProductDetail(props) {
  const [product, setProduct] = useState({});

  useEffect(() => {
    window.scrollTo(0, 0);
    const id = props.match.params.id;
    axios
      .get(`http://127.0.0.1:8000/api/products/${id}/`)
      .then((res) => {
        setProduct(res.data);
      })
      .catch((err) => {
      });
  }, [props.match.params.id]);

  return (
    <React.Fragment>
      <Helmet>
        <title>{`${product.title}`} - Ruby Online Hub</title>
        <meta
          name="description"
          content="Ruby Online Hub is a convenient forum for online distribution that promises secure and effective support for its customers. It is a verified online delivery platform for our customers to purchase fashion products conveniently."
        ></meta>
      </Helmet>
      <Detail product={product} />
      <Inquiry_Modal product={product} />
    </React.Fragment>
  );
}

export default ProductDetail;
