import React from "react";
import { Helmet } from "react-helmet";


function About() {
  return (
    <section className="bg-dark text-white pt-5">
      <Helmet>
        <title>About Us - Ruby Online Hub</title>
        <meta
          name="description"
          content="Ruby Online Hub is a convenient forum for online distribution that promises secure and effective support for its customers. It is a verified online delivery platform for our customers to purchase fashion products conveniently."
        ></meta>
      </Helmet>
      <div className="container">
        <div className="row text-center">
          <div className="col-md-12">
            <h2 className="text-center">About Us</h2>
            <p>
              Ruby Online Hub is a convenient forum for online distribution that
              promises secure and effective support for its customers. It is a
              verified online delivery platform for our customers to purchase
              fashion products conveniently Clients can easily order the
              delivered products in a relatively very fast time interval. Ruby
              Online Hub not only focuses on the trendy products on the market
              but also provides for the high quality of the products ordered.
              With our slogan "Customers are our God," we are a team of
              dedicated members always willing to serve our loved customers.
            </p>
          </div>
        </div>
      </div>
    </section>
  );
}

export default About;
