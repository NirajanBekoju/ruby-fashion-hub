import React from 'react'
import Banner from '../Components/Banner'
import FeaturedProduct from '../Components/FeaturedProduct'

import { Helmet } from 'react-helmet'

function Home() {
    return (
      <React.Fragment>
        <Helmet>
          <title>Home - Ruby Online Hub</title>
          <meta
            name="description"
            content="Ruby Online Hub is a convenient forum for online distribution that promises secure and effective support for its customers. It is a verified online delivery platform for our customers to purchase fashion products conveniently."
          ></meta>
        </Helmet>
        <Banner />
        <FeaturedProduct />
      </React.Fragment>
    );
}

export default Home
