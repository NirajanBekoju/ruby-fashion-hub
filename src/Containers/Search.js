import React, {useState, useEffect} from 'react'
import axios from 'axios'
import { Helmet } from "react-helmet";

import ProductCard from '../Components/ProductCard'

function Search(props) {
  const keyword = props.location.search.slice(3);

  // Here listings contains only 9 products since the products per page in the backend is 9.
  const [listings, setListings] = useState([]);

  useEffect(() => {
    window.scrollTo(0, 0);
    const fetchData = async () => {
      try {
        const res = await axios.post(
          "http://127.0.0.1:8000/api/products/search", {keyword}
        );
        setListings(res.data);
      } catch (err) {

      }
    };

    fetchData();
  }, []);

  const displayListings = () => {
      let display = []
      let result = []

      listings.map((listing) => {
          return display.push(<ProductCard product={listing} />)
      });

      for (let i = 0; i<listings.length; i+=4){
        result.push(
          <div className="row" key={i}>
            <div className="col-md-3">{display[i]}</div>
            <div className="col-md-3">
              {display[i + 1] ? display[i + 1] : null}
            </div>
            <div className="col-md-3">
              {display[i + 2] ? display[i + 2] : null}
            </div>
            <div className="col-md-3">
              {display[i + 3] ? display[i + 3] : null}
            </div>
          </div>
        );
      }

      return result;
  }

  const noResults = () => {
      return (
          <div className="row">
              <div className="col-md-12">
                <h5>No Search Result</h5>
              </div>
          </div>
      )
  }

  return (
    <section className="bg-light pt-4">
      <Helmet>
        <title>Search - Ruby Online Hub</title>
        <meta
          name="description"
          content="Ruby Online Hub is a convenient forum for online distribution that promises secure and effective support for its customers. It is a verified online delivery platform for our customers to purchase fashion products conveniently."
        ></meta>
      </Helmet>
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h2>Search Results</h2>
          </div>
        </div>
        {displayListings().length > 0 ? displayListings() : noResults()}
      </div>
    </section>
  );
}

export default Search
