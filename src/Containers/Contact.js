import React from 'react'
import { Helmet } from "react-helmet";

import ContactForm from '../Components/Contact/ContactForm';
import Map from '../Components/Contact/Map';

function Contact() {
    return (
      <React.Fragment>
        <Helmet>
          <title>Contact Us - Ruby Online Hub</title>
          <meta
            name="description"
            content="Ruby Online Hub is a convenient forum for online distribution that promises secure and effective support for its customers. It is a verified online delivery platform for our customers to purchase fashion products conveniently."
          ></meta>
        </Helmet>
        <ContactForm />
        <Map />
      </React.Fragment>
    );
}

export default Contact
