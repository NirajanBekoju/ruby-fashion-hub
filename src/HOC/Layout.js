import React from 'react'
import TopBar from '../Components/Bar/TopBar';
import NavBar from '../Components/Bar/NavBar';
import Footer from '../Components/Bar/Footer';
import CopyrightBar from '../Components/Bar/CopyrightBar';

const layout = (props) => (
    <div>
        <TopBar />
        <NavBar />
        {props.children}
        <Footer />
        <CopyrightBar />
    </div>
)

export default layout
