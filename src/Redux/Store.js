import { createStore } from "redux";

import alertReducer from './Alert/AlertReducers';

const store = createStore(alertReducer);

export default store;
