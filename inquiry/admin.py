from django.contrib import admin
from .models import Inquiry

# Register your models here.
class InquiryAdmin(admin.ModelAdmin):
    list_display = ('id','name', 'product_id', 'email', 'phone', 'inquiry_date')
    list_display_links = ('id', 'name')
    search_fields = ('name', 'email', 'product_id', 'phone')
    list_per_page = 30

admin.site.register(Inquiry, InquiryAdmin)