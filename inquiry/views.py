from rest_framework import permissions
from rest_framework.views import APIView
from .models import Inquiry
from rest_framework.response import Response
from products.models import Product
from .serializer import InquirySerializer

class InquiryCreateView(APIView):
    permission_classes = (permissions.AllowAny, )

    def post(self, request, format=None):
        data = self.request.data
        try:
            product = Product.objects.filter(id=data['product_id'])
            if(product[0]):
                serializer = InquirySerializer(data=data)
                if(serializer.is_valid()):
                    serializer.save()
                    return Response({'success': 'Message Sent Successfully'})
                else:
                    return Response({'error': 'Message Failed to sent'})
            else:
                return Response({'error': 'Message Failed to sent'})
        except:
            return Response({'error': 'Message Failed to sent'})
       
        
