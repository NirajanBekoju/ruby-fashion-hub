# Generated by Django 3.1.6 on 2021-02-16 04:41

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contact', '0002_auto_20210216_0409'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contact',
            name='phone',
            field=models.CharField(max_length=12, validators=[django.core.validators.RegexValidator(message='Phone number must be entered in the format: 98*-***-****', regex='^(\\+\\d{1,2}\\s)?\\(?\\d{3}\\)?[\\s.-]\\d{3}[\\s.-]\\d{4}$')]),
        ),
    ]
