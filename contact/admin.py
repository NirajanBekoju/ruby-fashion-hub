from django.contrib import admin
from .models import Contact

# Register your models here.
class ContactAdmin(admin.ModelAdmin):
    list_display = ('id','name', 'email', 'address', 'phone', 'contact_date')
    list_display_links = ('id', 'name')
    search_fields = ('name', 'email', 'address','phone')
    list_per_page = 30

admin.site.register(Contact, ContactAdmin)