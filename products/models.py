from django.db import models
from django.utils.timezone import now

# Create your models here.
class Product(models.Model):
    title = models.CharField(max_length=50, unique=True, blank=False)
    slug = models.CharField(max_length=50, unique=True ,blank=False)
    image = models.ImageField(upload_to='photos/%Y/%m/%d/', blank=False)
    description = models.TextField(blank=False)
    cut_price = models.IntegerField(blank=True)
    price = models.IntegerField(blank=False)
    is_offered = models.BooleanField(default=False)
    is_featured = models.BooleanField(default=False)
    is_published = models.BooleanField(default=True)
    created_date = models.DateTimeField(default=now, blank=True)

    def __str__(self):
        return self.title
    

