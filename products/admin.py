from django.contrib import admin
from .models import Product

# Register your models here.
class ProductAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'cut_price', 'price', 'is_offered', 'is_featured', 'is_published', 'created_date')
    list_display_links = ('id', 'title')
    list_filter = ('is_published', 'is_featured', 'is_offered')
    list_editable = ('is_published', 'is_featured', 'is_offered')
    search_fields = ('title', 'description')
    list_per_page = 30

admin.site.register(Product, ProductAdmin)