from django.urls import path
from .views import ListingsProductView, ListingProductDetailView, FeaturedProductView, OfferedProductView, SearchProductView

urlpatterns = [
    path('', ListingsProductView.as_view()),
    path('search', SearchProductView.as_view()),
    path('featured/', FeaturedProductView.as_view()),
    path('offered/', OfferedProductView.as_view()),
    path('<id>/', ListingProductDetailView.as_view()),
]