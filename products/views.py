from rest_framework.response import Response
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.views import APIView
from rest_framework import permissions
from .models import Product
from .serializers import ProductSerializer, ProductDetailSerializer, FeaturedProductSerializer, OfferedProductSerializer
from rest_framework.pagination import PageNumberPagination

class StandardResultsSetPagination(PageNumberPagination):
    page_size = 8
    page_size_query_param = 'page_size'
    max_page_size = 1000

class ListingsProductView(ListAPIView, PageNumberPagination):
    queryset = Product.objects.order_by('-created_date').filter(is_published=True)
    permission_classes = (permissions.AllowAny, )
    serializer_class = ProductSerializer
    lookup_field = 'id'
    pagination_class = StandardResultsSetPagination

class FeaturedProductView(ListAPIView):
    queryset = Product.objects.order_by('-created_date').filter(is_featured=True)
    permission_classes=(permissions.AllowAny, )
    serializer_class = FeaturedProductSerializer
    lookup_field = 'id'

class OfferedProductView(ListAPIView):
    queryset = Product.objects.order_by('-created_date').filter(is_offered=True)
    permission_classes=(permissions.AllowAny, )
    serializer_class = OfferedProductSerializer
    lookup_field = 'id'

class ListingProductDetailView(RetrieveAPIView):
    queryset = Product.objects.order_by('-created_date').filter(is_published=True)
    permission_classes=(permissions.AllowAny, )
    serializer_class = ProductDetailSerializer
    lookup_field = 'id'

class SearchProductView(APIView):
    permission_classes = (permissions.AllowAny, )
    serializer_class = ProductSerializer

    def post(self, request, format=None):
        queryset = Product.objects.order_by('-created_date').filter(is_published=True)
        data = self.request.data

        keyword = data['keyword']
        queryset = queryset.filter(title__icontains=keyword)
        
        serializers = ProductSerializer(queryset, many=True)
        return Response(serializers.data)




